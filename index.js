const horseRaceGame = new HorseRacing();

function HorseRacing() {
    const PLAYER_DEFAULT_BANK = 1000;
    this.horses = [
        // заполните список лошадей
    ];
    this.player = new Player(PLAYER_DEFAULT_BANK);
    this.wagers = [];
    this.clearWagers = function () {
        // очистить ставки
    };

    function Horse(name) {
        this.name = name;
        this.run = function () {
            // вернуть Promise, который будет автоматически выполнен через случайное время
        };
    }

    function Player(bank) {
        this.account = bank;
    }

    // здесь вы можете дописать различные вспомогательные функции
    // например, для проверки ставок, изменения счета игрока и т.д.
}

function showHorses() {
    // вывести список лошадей
}

function showAccount() {
    // показать сумму на счету игрока
}

function setWager(name, sumToBet) {
    // сделать ставку на лошадь в следующем забеге
}

function startRacing() {
    // начать забег
}

function newGame() {
    // восстановить исходный счет игрока
}
